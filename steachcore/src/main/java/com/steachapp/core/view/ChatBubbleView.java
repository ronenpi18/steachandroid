package com.steachapp.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.steachapp.core.R;

public class ChatBubbleView extends LinearLayout {

    private static final String TAG = ChatBubbleView.class.getSimpleName();

    public static final int MESSAGE_SENT = 0;
    public static final int MESSAGE_RECEIVED = 1;

    private String mBubbleMessage;
    private int mBubbleColor, mTextColor;
    private int mBubbleType;

    private TextView mBubbleMessageTV;
    private ImageView mProfileIV;

    public ChatBubbleView(Context context) {
        super(context);
        init(context, null);
    }

    public ChatBubbleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.message_bubble, this, true);
        setOrientation(HORIZONTAL);

        Drawable profileImageDrawable = null;

        if(attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.ChatBubbleView,
                    0, 0);
            try {
                mBubbleMessage = a.getString(R.styleable.ChatBubbleView_bubbleMessage);
                int blue = getResources().getColor(R.color.md_blue_400);
                int white = getResources().getColor(R.color.md_white_1000);
                mBubbleColor = a.getColor(R.styleable.ChatBubbleView_bubbleColor, blue);
                mBubbleType = a.getInt(R.styleable.ChatBubbleView_bubbleType, MESSAGE_SENT);
                mTextColor = a.getColor(R.styleable.ChatBubbleView_bubbleTextColor, white);
                try {
                    profileImageDrawable = a.getDrawable(R.styleable.ChatBubbleView_bubbleProfileImage);
                } catch (UnsupportedOperationException ex) {
                    Log.e(TAG, "Profile image not set - value is not a Drawable or a Color");
                }
            } finally {
                a.recycle();
            }
        }

        mBubbleMessageTV = (TextView)findViewById(R.id.message_bubble_text);
        mProfileIV = (ImageView)findViewById(R.id.message_bubble_profile_image);

        setBackgroundDrawable(getResources().getDrawable(R.drawable.bubble));
        setMessage(mBubbleMessage);
        setBubbleType(mBubbleType);
        setBubbleColor(mBubbleColor);
        setBubbleTextColor(mTextColor);

        if(profileImageDrawable != null) {
            setProfileImage(profileImageDrawable);
        }
    }

    public void setMessage(String message) {
        mBubbleMessage = message;
        mBubbleMessageTV.setText(mBubbleMessage);
        invalidate();
        requestLayout();
    }

    public String getMessage() {
        return mBubbleMessage;
    }

    public void setBubbleColor(@ColorInt int color) {
        mBubbleColor = color;
        mBubbleMessageTV.getBackground().setColorFilter(mBubbleColor, PorterDuff.Mode.SRC_ATOP);
        invalidate();
        requestLayout();
    }

    public @ColorInt int getBubbleColor() { return mBubbleColor; }

    public void setBubbleTextColor(@ColorInt int color) {
        mTextColor = color;
        mBubbleMessageTV.setTextColor(mTextColor);
        invalidate();
        requestLayout();
    }

    public @ColorInt int getBubbleTextColor() { return mTextColor; }

    public void setBubbleType(int type) {
        mBubbleType = type;
        Drawable bg;
        switch (mBubbleType) {
            case MESSAGE_SENT:
                bg = getResources().getDrawable(R.drawable.bubble);
                mProfileIV.setVisibility(GONE);
                break;
            case MESSAGE_RECEIVED:
                bg = getResources().getDrawable(R.drawable.bubble_received);
                mProfileIV.setVisibility(VISIBLE);
                break;
            default:
                throw new IllegalArgumentException("Bubble type must be either MESSAGE_SENT or MESSAGE_RECEIVED");
        }
        setBackgroundDrawable(bg);
        invalidate();
        requestLayout();
    }

    public int getBubbleType() { return mBubbleType; }

    @Override
    public void setBackgroundDrawable(Drawable background) {
        if(mBubbleMessageTV != null) {
            mBubbleMessageTV.setBackgroundDrawable(background);
            mBubbleMessageTV.getBackground().setColorFilter(mBubbleColor, PorterDuff.Mode.SRC_ATOP);
        }
    }

    public void setProfileImage(Bitmap bmp) { mProfileIV.setImageBitmap(bmp); }

    public void setProfileImage(Drawable drawable) { mProfileIV.setImageDrawable(drawable); }

    public ImageView getProfileImage() {return mProfileIV; }
}
