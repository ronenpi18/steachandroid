package com.steachapp.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.steachapp.core.R;

public class PagerIndicatorDot extends ImageView {

    /**
     * Holds the state of the dot, true if selected, false otherwise
     */
    private boolean mIsDotSelected;
    /**
     * Size of the dot, defaulted to 20x20 dp
     */
    private float mDotSize;

    /** {@inheritDoc} */
    public PagerIndicatorDot(Context context) {
        super(context);
        init(context, null);
    }

    /** {@inheritDoc} */
    public PagerIndicatorDot(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /** {@inheritDoc} */
    public PagerIndicatorDot(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * Initiate the view class, set custom attributes like size and state
     * @param c The context of the view
     * @param attrs The {@link AttributeSet} of the view
     */
    private void init(Context c, AttributeSet attrs) {
        if(attrs != null) {
            TypedArray a = c.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.PagerIndicatorDot,
                    0, 0);

            try {
                setDotSelected(a.getBoolean(R.styleable.PagerIndicatorDot_dotSelected, false));
                mDotSize = a.getDimension(R.styleable.PagerIndicatorDot_dotSize, 20f);
            } finally {
                a.recycle();
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int px = (int)(getResources().getDisplayMetrics().density * mDotSize);
        setMeasuredDimension(px, px);
    }

    /**
     * Set the state of the indicator dot
     * @param selected True if the dot is selected, or false otherwise
     */
    public void setDotSelected(boolean selected) {
        mIsDotSelected = selected;
        Drawable d = selected ? getResources().getDrawable(R.drawable.page_dot_selected) :
                getResources().getDrawable(R.drawable.page_dot_normal);
        setImageDrawable(d);
        invalidate();
        requestLayout();
    }

    /**
     * Check if the dot is currently selected or not
     * @return True if the dot is selected, false otherwise
     */
    public boolean isSelected() { return mIsDotSelected; }

    public void setDotSize(float size) {
        mDotSize = size;
        invalidate();
        requestLayout();
    }

    public float getDotSize() { return mDotSize; }
}
