package com.steachapp.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.steachapp.core.R;

public class PagerIndicatorDotStrip extends LinearLayout {

    private int mSelectedDotPosition;
    private int mNumDots;

    public PagerIndicatorDotStrip(Context context) {
        super(context);
        init(context, null);
    }

    public PagerIndicatorDotStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PagerIndicatorDotStrip(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * Initiate the view group class, set custom attributes
     * @param c The context of the view
     * @param attrs The {@link AttributeSet} of the view
     */
    private void init(Context c, AttributeSet attrs) {
        mNumDots = 0;
        mSelectedDotPosition = 0;
        if(attrs != null) {
            TypedArray a = c.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.PagerIndicatorDotStrip,
                    0, 0);

            mSelectedDotPosition = a.getInt(R.styleable.PagerIndicatorDotStrip_selectedPosition, -1);
            int numDots = a.getInt(R.styleable.PagerIndicatorDotStrip_numDots, 0);
            if(mSelectedDotPosition >= numDots) {
                mSelectedDotPosition = numDots - 1;
                //TODO Handle
//                throw new IllegalArgumentException("Position of selected dot indicator cannot exceed number of dots");
            }
            if(mSelectedDotPosition == -1) mSelectedDotPosition = 0;
            setNumDots(numDots);
        }
    }

    @Override
    public void addView(@NonNull View child) {
        if(child instanceof PagerIndicatorDot) {
            super.addView(child);
        }
    }

    /**
     * Sets the number of dots in the strip
     * @param numDots The number of dots in the strip
     */
    public void setNumDots(int numDots) {
        addDots(numDots - mNumDots);
        mNumDots = numDots;
    }

    /**
     * Gets the number of dots in the dot strip
     * @return How many dots there are
     */
    public int getNumDots() { return mNumDots; }

    private PagerIndicatorDot generateDot(int margin, boolean selected) {
        PagerIndicatorDot dot = new PagerIndicatorDot(getContext());
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.bottomMargin = margin;
        params.leftMargin = margin;
        params.topMargin = margin;
        params.rightMargin = margin;
        dot.setLayoutParams(params);
        dot.setDotSelected(selected);
        dot.setDotSize(20);
        return dot;
    }

    private void addDots(int num) {
        if (num < 0) {
            removeViews(0, num);
            setSelectedDot(Math.min(getSelectedDotPosition(), getChildCount() - 1));
        } else {
            for (int i = 0; i < num; i++) {
                int margin = 8;
                PagerIndicatorDot dot = generateDot(margin, false);
                addView(dot);
            }
        }
        invalidate();
        requestLayout();
    }

    /**
     * Set the selected dot
     * @param position The position of the dot
     */
    public void setSelectedDot(int position) {
        mSelectedDotPosition = position;
        for(int i = 0; i < getChildCount(); i++) {
            ((PagerIndicatorDot)getChildAt(i)).setDotSelected(false);
        }
        ((PagerIndicatorDot)getChildAt(position)).setDotSelected(true);
    }

    /**
     * Get the position of the currently selected indicator dot
     * @return An index from 0 to n-1 of the currently selected indicator dot
     */
    public int getSelectedDotPosition() { return mSelectedDotPosition; }

    public void setupWithViewPager(ViewPager pager) {
        PagerAdapter adapter = pager.getAdapter();
        if(adapter == null) {
            return; //TODO Handle
        }
        setNumDots(adapter.getCount());
        setSelectedDot(0);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
            @Override public void onPageScrollStateChanged(int state) { }
            @Override public void onPageSelected(int position) {
                setSelectedDot(position);
            }

        });
    }
}
