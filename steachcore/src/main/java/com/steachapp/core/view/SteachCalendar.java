package com.steachapp.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.steachapp.core.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by miki on 12/27/15.
 */
public class SteachCalendar extends LinearLayout{

    private static final String TAG = SteachCalendar.class.getSimpleName();

    private Calendar mCalendar;

    private GridView mCalendarGridView;

    private int mDefaultMonth;

    public SteachCalendar(Context context) {
        super(context);
        init(context, null);
    }

    public SteachCalendar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SteachCalendar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mCalendar = new GregorianCalendar();

        LayoutInflater.from(getContext()).inflate(R.layout.calendar, this, true);
        setOrientation(VERTICAL);
        setBackgroundColor(getResources().getColor(R.color.md_grey_100));

        mCalendarGridView = (GridView)findViewById(R.id.calender_view_month_grid);

        if(attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.SteachCalendar,
                    0, 0);
            try {
                mDefaultMonth = a.getInteger(R.styleable.SteachCalendar_calenderDefaultMonth,
                        mCalendar.get(Calendar.MONTH));
                mCalendar.set(Calendar.MONTH, mDefaultMonth);
            } finally {
                a.recycle();
            }
        }

        mCalendarGridView.setAdapter(new CalendarAdapter(mCalendar));
        mCalendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CalendarAdapter adapter = ((CalendarAdapter)parent.getAdapter());
                adapter.togglePressed(adapter.getItem(position));
            }
        });
    }

    public void setDefaultMonth(int month) {
        mDefaultMonth = month;
        mCalendar.set(Calendar.MONTH, mDefaultMonth);
        invalidate();
        requestLayout();
    }

    public int getDefaultMonth() { return mDefaultMonth; }

    public void setSelected(int day, boolean selected) {
        CalendarAdapter adapter = ((CalendarAdapter)mCalendarGridView.getAdapter());
        adapter.setSelected(day, selected);
    }

    public void setPressed(int day, boolean pressed) {
        CalendarAdapter adapter = ((CalendarAdapter)mCalendarGridView.getAdapter());
        adapter.setPressed(day, pressed);
    }


    private class CalendarAdapter extends BaseAdapter {
        private Calendar mCalendar;

        private int[] dayIndexer;

        private boolean selected[];
        private boolean pressed[];

        private int firstDayOfWeek;

        public CalendarAdapter(Calendar calendar) {
            mCalendar = calendar;
            mCalendar.set(Calendar.DAY_OF_MONTH, 1);
            firstDayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);
            mCalendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));

            dayIndexer = new int[35];
            selected = new boolean[mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)];
            pressed = new boolean[mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)];

            for(int i = 1; i <= dayIndexer.length; i++) {
                if(i >= firstDayOfWeek) {
                    dayIndexer[i - 1] = i - firstDayOfWeek + 1;
                } else if(i < firstDayOfWeek) {
                    dayIndexer[i - 1] = -1;
                }
            }
        }

        @Override
        public int getCount() {
            return dayIndexer.length;
        }

        @Override
        public Integer getItem(int position) {
            return dayIndexer[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public void toggleSelected(int day) {
            this.selected[day - 1] = !this.selected[day - 1];
            notifyDataSetChanged();
        }

        public void setSelected(int day, boolean selected) {
            this.selected[day - 1] = selected;
            notifyDataSetChanged();
        }

        public void togglePressed(int day) {
            boolean state = !this.pressed[day - 1];
            this.pressed = new boolean[this.pressed.length];
            this.pressed[day - 1] = state;
            notifyDataSetChanged();
        }

        public void setPressed(int day, boolean pressed) {
            this.pressed = new boolean[this.pressed.length];
            this.pressed[day - 1] = pressed;
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.calender_day_item, null, false);
            }

            TextView label = (TextView)convertView.findViewById(R.id.calender_day_item_textview);
            ImageView background = (ImageView)convertView.findViewById(R.id.calender_day_item_background);

            int daysInMonth = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

            if(getItem(position) > daysInMonth) {
                label.setText(String.valueOf(getItem(position) % daysInMonth));
                label.setTextColor(getResources().getColor(R.color.md_grey_500));
            } else if(getItem(position) < 0) {
                label.setText(".");
                label.setTextColor(getResources().getColor(R.color.md_grey_500));
            } else {
                label.setText(String.valueOf(getItem(position)));
                background.setSelected(selected[getItem(position) - 1]);
                background.setPressed(pressed[getItem(position) - 1]);
            }

//            label.setSelected(true);
//            background.setPressed(true);

            return convertView;
        }
    }
}
