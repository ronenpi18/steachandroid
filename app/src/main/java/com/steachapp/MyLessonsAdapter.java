package com.steachapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by miki on 12/29/15.
 */
public class MyLessonsAdapter extends RecyclerView.Adapter<MyLessonsAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    public MyLessonsAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(mInflater.inflate(R.layout.lessons_list_item, null));

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date, title, syllabus, dayOfWeek;

        public ViewHolder(View itemView) {
            super(itemView);

            date = (TextView)itemView.findViewById(R.id.lessons_list_item_date_full_textview);
            dayOfWeek = (TextView)itemView.findViewById(R.id.lessons_list_item_date_day_textview);
            syllabus = (TextView)itemView.findViewById(R.id.lessons_list_item_syllabus_textview);
            title = (TextView)itemView.findViewById(R.id.lessons_list_item_title_textview);
        }
    }
}
