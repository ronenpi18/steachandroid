package com.steachapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditProfileActivity extends MaterialActivity {

    @Bind(R.id.edit_profile_container)
    FrameLayout mContainerLayout;

    boolean mIsTeacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        mIsTeacher = true;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.edit_profile_container, new EditProfileFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    class EditProfileFragment extends Fragment {

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            if(mIsTeacher) {
                return inflater.inflate(R.layout.layout_edit_profile_teacher, null, false);
            } else {
//                return inflater.inflate(R.layout.layout_edit_profile_student);
            }
            return null;
        }
    }
}
