package com.steachapp;

import android.os.Bundle;

import com.steachapp.core.view.SteachCalendar;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BookLessonActivity extends MaterialActivity {

    @Bind(R.id.book_lesson_calendar)
    SteachCalendar mBookLessonCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_lesson);
        setToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);

        mBookLessonCalendar.setPressed(17, true);
        mBookLessonCalendar.setSelected(23,true);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }
}
