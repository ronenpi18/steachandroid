package com.steachapp.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by User on 16/02/2016.
 */
public class TutorialViewPager extends ViewPager implements ViewPager.OnPageChangeListener {

    private boolean isPagingEnabled;

    private int mScrollDirection;

    private GestureDetector.SimpleOnGestureListener mGestureListener;
    private GestureDetector mGestureDetector;

    private void init(Context c) {
        mGestureListener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                mScrollDirection = distanceX < 0 ? -1 : 1;
                return true;
            }
        };
        mGestureDetector = new GestureDetector(c, mGestureListener);
    }

    public TutorialViewPager(Context context) {
        super(context);
        isPagingEnabled = false;
        init(context);
    }

    public TutorialViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        isPagingEnabled = false;
        init(context);
    }

    public int getScrollDirection() {
        return mScrollDirection;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return this.isPagingEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }

    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }

    @Override
    public void onPageScrolled(int position, float offset, int offsetPixels) {
        super.onPageScrolled(position, offset, offsetPixels);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
