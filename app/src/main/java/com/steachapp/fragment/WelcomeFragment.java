package com.steachapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.steachapp.R;
import com.steachapp.TutorialActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by miki on 2/19/16.
 */
public class WelcomeFragment extends Fragment implements View.OnClickListener{

    public static final String TAG = WelcomeFragment.class.getSimpleName();

    public static final String SELECTED_USER_TYPE = "com.steachapp.fragment.SELECTED_USER_TYPE";
    public static final int USER_TYPE_STUDENT = 1;
    public static final int USER_TYPE_TEACHER = 2;

    TutorialActivity mParentActivity;


    @Bind(R.id.welcome_button_select_teacher)
    Button mSelectTeacherButton;
    @Bind(R.id.welcome_button_select_student)
    Button mSelectStudentButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() instanceof TutorialActivity) {
            mParentActivity = (TutorialActivity)getActivity();
//            mParentActivity.setStatusBarColor(Color.TRANSPARENT, true);
            mSelectTeacherButton.setOnClickListener(this);
            mSelectStudentButton.setOnClickListener(this);
        } else {
            //Should never occur unless some stupid programmer goes to change this code blindly
            Log.e(TAG, "Error binding WelcomeFragment to activity");
        }
    }

    @Override
    public void onClick(View v) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        switch (v.getId()) {
            case R.id.welcome_button_select_student:
                args.putInt(SELECTED_USER_TYPE, USER_TYPE_STUDENT);
                fragment.setArguments(args);
                mParentActivity.switchFragment(fragment);
                break;
            case R.id.welcome_button_select_teacher:
                args.putInt(SELECTED_USER_TYPE, USER_TYPE_TEACHER);
                fragment.setArguments(args);
                mParentActivity.switchFragment(fragment);
                break;
        }
    }
}
