package com.steachapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.steachapp.R;

/**
 * Created by miki on 12/29/15.
 */
public class ProfileFragment extends Fragment {

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, null, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

//        mRecyclerView.setAdapter(new MyLessonsAdapter(getContext()));
        //TODO add ChatsAdapter

        super.onActivityCreated(savedInstanceState);
    }
}
