package com.steachapp.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.steachapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FourthTutorialFragment extends Fragment implements View.OnClickListener {

    @Bind(R.id.fragment_fourth_facebook_info_tv)
    TextView mFacebookInfoTV;

    @Bind(R.id.tutorial_button_facebook_login)
    Button mFacebookLoginButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fourth, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFacebookInfoTV.setClickable(true);
        mFacebookInfoTV.setOnClickListener(this);

        mFacebookLoginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_fourth_facebook_info_tv:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AppTheme_Widget_AlertDialog);
                builder.setTitle("Info")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setMessage("Facebook disclaimer here");
                AlertDialog dialog = builder.show();
                break;
            case R.id.tutorial_button_facebook_login:
                loginWithFacebook();
        }

    }

    private void loginWithFacebook() {
        //TODO Implement FB Login here
    }
}
