package com.steachapp.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.steachapp.R;
import com.steachapp.TutorialActivity;
import com.steachapp.TutorialViewPagerAdapter;
import com.steachapp.core.view.PagerIndicatorDotStrip;
import com.steachapp.view.TutorialViewPager;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by miki on 2/20/16.
 */
public class TutorialFragment extends Fragment {

    @Bind(R.id.activity_tutorial_backdrop_switcher)
    ImageSwitcher mBackdropSwitcher;
    @Bind(R.id.activity_tutorial_view_pager)
    TutorialViewPager mTutorialViewPager;
    @Bind(R.id.tutorial_pager_dot_indicator_wrapper)
    PagerIndicatorDotStrip mDotIndicatorStrip;

    private TutorialActivity mParentActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tutorial, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() instanceof TutorialActivity) {
            mParentActivity = (TutorialActivity)getActivity();
//            mParentActivity.setStatusBarColor(Color.TRANSPARENT, true);

            Fragment[] fragments = new Fragment[] {
                    new FirstTutorialFragment(),
                    new SecondTutorialFragment(),
                    new ThirdTutorialFragment(),
                    new FourthTutorialFragment()
            };
            final Bitmap[] images = new Bitmap[] {
                    mParentActivity.getImageFromAssets("backdrops/desk.jpg"),
                    mParentActivity.getImageFromAssets("backdrops/screen_4.jpg"),
                    mParentActivity.getImageFromAssets("backdrops/desk_2.jpg"),
                    mParentActivity.getImageFromAssets("backdrops/desk_3.jpg"),
            };
            mBackdropSwitcher.setInAnimation(mParentActivity, android.R.anim.fade_in);
            mBackdropSwitcher.setOutAnimation(mParentActivity, android.R.anim.fade_out);
            mBackdropSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
                @Override
                public View makeView() {
                    ImageView imageView = new ImageView(mParentActivity.getApplicationContext());
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView.setLayoutParams(new ImageSwitcher.LayoutParams(ImageSwitcher.LayoutParams.MATCH_PARENT,ImageSwitcher.LayoutParams.MATCH_PARENT));
                    return imageView;
                }
            });

            mTutorialViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
                @Override public void onPageScrollStateChanged(int state) { }
                @Override
                public void onPageSelected(int position) {
                    mBackdropSwitcher.setImageDrawable(new BitmapDrawable(images[position]));
                }
            });
            TutorialViewPagerAdapter adapter = new TutorialViewPagerAdapter(fragments, getChildFragmentManager());
            mTutorialViewPager.setAdapter(adapter);
            mDotIndicatorStrip.setupWithViewPager(mTutorialViewPager);
            mTutorialViewPager.setOffscreenPageLimit(3);
            mTutorialViewPager.setPagingEnabled(true);
            mBackdropSwitcher.setImageDrawable(new BitmapDrawable(images[mTutorialViewPager.getCurrentItem()]));
        }
    }
}
