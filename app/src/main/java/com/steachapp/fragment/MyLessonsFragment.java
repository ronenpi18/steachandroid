package com.steachapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.steachapp.MyLessonsAdapter;
import com.steachapp.R;

/**
 * Created by miki on 12/29/15.
 */
public class MyLessonsFragment extends Fragment {

    private RecyclerView mRecyclerView;

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_lessons, null, false);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.my_lessons_recyclerview);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(new MyLessonsAdapter(getContext()));

        super.onActivityCreated(savedInstanceState);
    }
}
