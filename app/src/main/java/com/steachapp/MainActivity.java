package com.steachapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.appevents.AppEventsLogger;
import com.steachapp.fragment.ChatsFragment;
import com.steachapp.fragment.MyLessonsFragment;
import com.steachapp.fragment.ProfileFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends MaterialActivity {

    @Bind(R.id.activity_main_view_pager)
    ViewPager mMainViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        setStatusBarColorResource(R.color.colorPrimaryDark, false);

        int activitySwitchState = 1;

        switch (activitySwitchState) {
            case 0:
                startActivity(new Intent(this, TutorialActivity.class));
                finish();
                break;
            default:
                setToolbar(R.id.toolbar);
                setTabLayout(R.id.tab_layout);
                setTitle("");

                final Fragment[] fragments = new Fragment[] {
                        new ChatsFragment(),
                        new MyLessonsFragment(),
                        new ProfileFragment()
                };
                mMainViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
                    @Override
                    public Fragment getItem(int position) {
                        return fragments[position];
                    }

                    @Override
                    public int getCount() {
                        return fragments.length;
                    }
                });

                TabLayout tabLayout = getTabLayout();
                if(tabLayout != null) {
                    tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_chat));
                    tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_calendar), true);
                    tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_profile));

                    mMainViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                    tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mMainViewPager));
                    if(mMainViewPager.getAdapter().getCount() > 0) {
                        TabLayout.Tab tab = tabLayout.getTabAt(mMainViewPager.getAdapter().getCount() / 2);
                        if(tab != null) tab.select();
                    }
                }
                break;
        }

    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_vp_activity:
                startActivity(new Intent(this, ViewProfileActivity.class));
                return true;
            case R.id.action_tutorial_activity:
                startActivity(new Intent(this, TutorialActivity.class));
                return true;
            case R.id.action_chat_activity:
                startActivity(new Intent(this, ChatActivity.class));
                return true;
            case R.id.action_view_chats_activity:
                startActivity(new Intent(this, ViewChatsActivity.class));
                return true;
            case R.id.action_find_subjects_activity:
                startActivity(new Intent(this, FindSubjectActivity.class));
                return true;
            case R.id.action_book_lesson_activity:
                startActivity(new Intent(this, BookLessonActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }
}

