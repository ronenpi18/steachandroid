package com.steachapp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FindSubjectActivity extends MaterialActivity implements TextWatcher, AdapterView.OnItemClickListener{

    LinearLayout searchContainer;
    EditText toolbarSearchView;
    ImageView searchClearButton;

    @Bind(R.id.activity_find_subject_list)
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_subject);
        initSearch();
        setToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);
        setTitle("");

        //TODO Add backend data to adapter
        List<String> names = Arrays.asList("Math", "Physics", "Computer Science", "Chemistry", "Biology");
        Map<String, Bitmap> map = new LinkedHashMap<>();
        for(int i = 0; i < names.size(); i++) {
            Bitmap bmp = Utils.getBitmapFromAsset(this, "subjects/" + names.get(i) + ".jpg");
            map.put(names.get(i), bmp == null ? bmp : Utils.fastblur(bmp, 0.7f, 15));
        }

        mListView.setAdapter(new SubjectAdapter(map));
        mListView.setOnItemClickListener(this);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    private void initSearch() {
        searchContainer = (LinearLayout)findViewById(R.id.search_container);
        toolbarSearchView = (EditText) findViewById(R.id.search_view);
        searchClearButton = (ImageView) findViewById(R.id.search_clear);

        toolbarSearchView.addTextChangedListener(this);
        searchClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchView.setText("");
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //TODO Subject item click
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //TODO On search query text changed
    }

    @Override
    public void afterTextChanged(Editable s) {}

    class SubjectAdapter extends BaseAdapter {

        private List<String> mData;
        private List<Bitmap> mImageData;
        private LayoutInflater mInflater;

        public SubjectAdapter(Map<String, Bitmap> data) {
            mData = new ArrayList<>();
            mImageData = new ArrayList<>();
            mInflater = getLayoutInflater();

            for(Map.Entry<String, Bitmap> entry : data.entrySet()) {
                mData.add(entry.getKey());
                mImageData.add(entry.getValue());
            }

        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public String getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = mInflater.inflate(R.layout.card_subject, null, false);
            }

            ImageView backdrop = (ImageView)convertView.findViewById(R.id.card_subject_backdrop);
            TextView name = (TextView)convertView.findViewById(R.id.card_subject_name);

            if(position < mImageData.size() && mImageData.get(position) != null) {
                backdrop.setImageBitmap(mImageData.get(position));
            }

            name.setText(getItem(position));

            return convertView;
        }
    }
}
