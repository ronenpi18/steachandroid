package com.steachapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.steachapp.core.view.ChatBubbleView;

public class ChatBubbleAdapter extends RecyclerView.Adapter<ChatBubbleAdapter.BubbleViewHolder> {

    private String[] mData;
    private Context mContext;
    private LayoutInflater mInflater;

    public ChatBubbleAdapter(Context c, String[] data) {
        mData = data;
        mContext = c;
        mInflater = LayoutInflater.from(mContext);
    }

    public String getMessage(int position) {
        return mData[position];
    }

    @Override
    public int getItemCount() {
        return mData.length;
    }

    @Override
    public void onBindViewHolder(BubbleViewHolder holder, int position) {
        final String url = "https://blog.neighborly.com/wp-content/uploads/2015/03/sean_profile_picture_round.png";
        int type = position % 2;
        holder.chatBubble.setMessage(getMessage(position));
        holder.chatBubble.setBubbleType(type);
        if(holder.chatBubble.getProfileImage().getDrawable() == null) {
            Picasso.with(mContext).load(url).error(R.mipmap.ic_launcher).into(holder.chatBubble.getProfileImage());
        }
    }

    @Override
    public BubbleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.message_bubble_list_item, null);
        return new BubbleViewHolder(view);
    }

    class BubbleViewHolder extends RecyclerView.ViewHolder {

        public ChatBubbleView chatBubble;

        public BubbleViewHolder(View itemView) {
            super(itemView);
            chatBubble = (ChatBubbleView)itemView.findViewById(R.id.chat_bubble);
        }
    }
}
