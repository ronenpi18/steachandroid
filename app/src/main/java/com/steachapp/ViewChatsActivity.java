package com.steachapp;

import android.os.Bundle;
import android.view.Menu;

import butterknife.ButterKnife;

public class ViewChatsActivity extends MaterialActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_chats);
        setToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
