package com.steachapp;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Iterator;
import java.util.LinkedHashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ViewProfileActivity extends MaterialActivity {

//    @Bind(R.id.view_profile_info_listview)
//    ListView mPersonalInfoLV;
    @Bind(R.id.card_personal_info_linearlayout)
    LinearLayout mPersonalInfoLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        setToolbar(R.id.toolbar);
        setDisplayHomeAsUpEnabled(true);

        LinkedHashMap<String, String> testMap = new LinkedHashMap<>();
        testMap.put("From", "Rishon LeZion");
        testMap.put("Studies at", "Rishon LeZion, Ra'anana, Ashdod, Herzliya");
        testMap.put("Education", "PHD");
        testMap.put("Price per 45 minutes", "120NIS");
        PersonalInfoListAdapter adapter = new PersonalInfoListAdapter(testMap);
        setAdapter(adapter);
//        mPersonalInfoLV.setAdapter(adapter);
//        setListViewHeightBasedOnItems(mPersonalInfoLV);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    private void setAdapter(BaseAdapter adapter) {
        for(int i = 0 ; i < adapter.getCount();i++) {
            mPersonalInfoLayout.addView(adapter.getView(i, null, null));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets ListView height dynamically based on the height of the items.
     *
     * @param listView to be resized
     * @return true if the listView is successfully resized, false otherwise
     */
    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight() + item.getPaddingTop() + item.getPaddingBottom();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    class PersonalInfoListAdapter extends BaseAdapter {

        private LinkedHashMap<String, String> mData;

        public PersonalInfoListAdapter(LinkedHashMap<String, String> data) {
            mData = data;
        }

        @Override public int getCount() { return mData.size(); }

        @Override
        public String getItem(int position) {
            Iterator<String> it = mData.keySet().iterator();
            String val = it.next();
            for(int i = 0; i < position; i++) {
                val = it.next();
            }
            return val;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = View.inflate(ViewProfileActivity.this, R.layout.personal_info_item, null);
            }
            TextView headerTv = (TextView)convertView.findViewById(R.id.personal_info_item_header);
            TextView contentTv = (TextView)convertView.findViewById(R.id.personal_info_item_text);
            String header = getItem(position);
            headerTv.setText(header);
            contentTv.setText(mData.get(header));

            return convertView;
        }
    }
}
