package com.steachapp;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChatActivity extends MaterialActivity implements View.OnClickListener{

    @Bind(R.id.chat_add_photo_button)
    ImageButton mAddPhotoButton;
    @Bind(R.id.chat_send_message_button)
    ImageButton mSendButton;
    @Bind(R.id.activity_chat_recycler_view)
    RecyclerView mChatBubblesRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        setToolbar(R.id.toolbar);

        mAddPhotoButton.setOnClickListener(this);
        mSendButton.setOnClickListener(this);
        mChatBubblesRV.setLayoutManager(new GridLayoutManager(this, 1));
        String[] msgs = new String[50];

        for(int i = 0; i < 50; i++) {
            msgs[i] = i % 3 == 0 ? "This is a test message" :  "This is a long test message with more content";
        }
        mChatBubblesRV.setAdapter(new ChatBubbleAdapter(this,msgs));

        getToolbar().setTitle("Teacher name");
        setTitle("Teacher name");
        getToolbar().setSubtitle("Occupation");

        setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            LinearLayout bottomBar = (LinearLayout)findViewById(R.id.chat_bottom_bar);
            bottomBar.setBackgroundColor(getResources().getColor(R.color.md_grey_200));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_send_message_button:
                //TODO Send message
                break;
            case R.id.chat_add_photo_button:
                //TODO Add photo dialog
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
