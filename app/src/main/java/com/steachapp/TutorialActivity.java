package com.steachapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

import com.steachapp.fragment.TutorialFragment;
import com.steachapp.fragment.WelcomeFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TutorialActivity extends MaterialActivity {

    @Bind(R.id.activity_tutorial_button_exit)
    ImageButton mExitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.activity_tutorial_fragment, new WelcomeFragment()).commit();
        mExitButton.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tutorial, menu);
        return true;
    }

    public <T extends Fragment> void switchFragment(T fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.fragment_enter_right, R.anim.fragment_exit_left, R.anim.fragment_enter_left, R.anim.fragment_exit_right);
        transaction.replace(R.id.activity_tutorial_fragment, fragment);

        if(fragment instanceof TutorialFragment) {
            mExitButton.setVisibility(View.VISIBLE);
        }

//        setStatusBarColor(Color.BLACK, true);

        transaction.addToBackStack(fragment.getClass().getSimpleName()).commit();
    }

    @OnClick(R.id.activity_tutorial_button_exit)
    public void exit() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mExitButton.setVisibility(View.INVISIBLE);
    }
}
