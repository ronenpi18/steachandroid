package com.steachapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by miki on 11/29/15.
 */
public class TutorialViewPagerAdapter extends FragmentPagerAdapter{

    private Fragment[] mFragments;

    public TutorialViewPagerAdapter(Fragment[] frags, FragmentManager fm) {
        super(fm);
        mFragments = frags;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments[position];
    }

    @Override
    public int getCount() {
        return mFragments.length;
    }
}
