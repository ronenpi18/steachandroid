package com.steachapp;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

public abstract class MaterialActivity extends AppCompatActivity{

    public static final int STATUS_BAR_ANIMATION_DURATION_SHORT = 300;
    public static final int STATUS_BAR_ANIMATION_DURATION_LONG = 550;

    private int mNextEnterAnimation, mExitAnimation, mNextExitAnimation, mEnterAnimation;

    private Toolbar mToolbar;
    private TabLayout mTabLayout;

    /**
     * Use this function to initiate all views for this activity
     */
    protected abstract void initViews();

    /** {@inheritDoc} */
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initViews();
    }

    /** {@inheritDoc} */
    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        initViews();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        overrideFont("fonts/Rudiment.ttf");
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        overrideFont("fonts/Rudiment.ttf");
    }

    public void overrideFont(String assetName) {
        Toolbar t = getToolbar();
        TabLayout tl = getTabLayout();
        if(t != null) Utils.overrideFont(this, t, assetName);
        if(tl != null) Utils.overrideFont(this, tl, assetName);
    }

    /**
     * Sets the status bar color for devices that run version {@value Build.VERSION_CODES#LOLLIPOP} and up
     * @param colorResId The resource id of the color to change into
     * @param animateChange Whether to animate the color change or not
     */
    public void setStatusBarColorResource(@ColorRes int colorResId, boolean animateChange) {
        setStatusBarColor(getResources().getColor(colorResId), animateChange);
    }

    /**
     * Sets the status bar color for devices that run version {@value Build.VERSION_CODES#LOLLIPOP} and up
     * @param color The color to change into
     * @param animateChange Whether to animate the color change or not
     */
    public void setStatusBarColor(@ColorInt int color, boolean animateChange) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            if(animateChange) {
                animateStatusBarColor(window.getStatusBarColor(), color, STATUS_BAR_ANIMATION_DURATION_SHORT);
            } else {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        }
    }

    /**
     * Change the color of the status bar with animation
     * @param baseColor the current color of the status bar
     * @param color the color to change the status bar into
     * @param duration The duration of the animation
     */
    @SuppressLint("NewApi") private void animateStatusBarColor(final @ColorInt int baseColor, final @ColorInt int color, int duration) {

        final Window window = getWindow();
        ValueAnimator animator = ValueAnimator.ofFloat(0, 1);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float animatedFraction = animation.getAnimatedFraction();
                int blendedColor = getBlendColor(baseColor, color, animatedFraction);
                window.setStatusBarColor(blendedColor);
            }
        });

        animator.setDuration(duration).start();
    }

    private int getBlendColor(@ColorInt int baseColor, @ColorInt int targetColor, float ratio) {
        final float inverseRatio = 1f - ratio;
        final float r = Color.red(targetColor) * ratio + Color.red(baseColor) * inverseRatio;
        final float g = Color.green(targetColor) * ratio + Color.green(baseColor) * inverseRatio;
        final float b = Color.blue(targetColor) * ratio + Color.blue(baseColor) * inverseRatio;
        return Color.rgb((int) r, (int) g, (int) b);
    }

    /**
     * Set the {@link TabLayout} for this activity
     * @param tabLayout The tab layout
     */
    public void setTabLayout(TabLayout tabLayout) {
        mTabLayout = tabLayout;
    }

    /**
     * Set the {@link TabLayout} for this activity
     * @param resId The tab layout resource id
     */
    public void setTabLayout(int resId) {
        setTabLayout((TabLayout)findViewById(resId));
    }

    /**
     * Get the {@link TabLayout} for this activity
     * @return The {@link TabLayout} set for this activity, or null if one wasn't set
     */
    public TabLayout getTabLayout() { return mTabLayout; }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSupportActionBar(Toolbar toolbar) {
        mToolbar = toolbar;
        super.setSupportActionBar(toolbar);
    }

    /**
     * Set the toolbar for this activity
     * @param toolbar Resource id for the toolbar
     */
    public void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
    }

    /**
     * Set the toolbar for this activity
     * @param resId Resource id for the toolbar
     */
    public void setToolbar(int resId) { setToolbar((Toolbar) findViewById(resId)); }

    /**
     * Get the toolbar for this activity
     * @return The toolbar set for this activity, or null if one wasn't set
     */
    public Toolbar getToolbar() { return mToolbar; }

    /**
     * Set the enter and exit animations for activites that are accessed through this activity.
     * These animation are called when another activity exits to and enters from this activity
     * @param enterAnim Enter animation resource id
     * @param exitAnim Exit animation resource id
     */
    public void setNextActivityAnimation(int enterAnim, int exitAnim) {
        mNextEnterAnimation = enterAnim;
        mNextExitAnimation = exitAnim;
    }

    /**
     * Set the enter and exit animations for this activity.
     * These animations are called when this activity enters or exits the screen
     * @param enterAnim Enter animation resource id
     * @param exitAnim Exit animation resource id
     */
    public void setActivityAnimation(int enterAnim, int exitAnim) {
        mEnterAnimation = enterAnim;
        mExitAnimation = exitAnim;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void finish() {
        super.finish();
        if(mNextEnterAnimation != 0 && mExitAnimation != 0) {
            overridePendingTransition(mNextEnterAnimation, mExitAnimation);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(mNextExitAnimation != 0 && mEnterAnimation != 0) {
            overridePendingTransition(mNextExitAnimation, mEnterAnimation);
        }
    }

    /**
     * Enable or disable the home button on the toolbar with some customization
     * @param enabled Set the home button to be enabled or disabled
     * @param drawableResId A resource id for the button {@link Drawable}
     * @param drawableColorFilter A color resource to use as a filter for the drawable
     */
    public void setDisplayHomeAsUpEnabled(boolean enabled, int drawableResId, int drawableColorFilter) {
        if(getSupportActionBar() == null) return;
        final Drawable upArrow = getResources().getDrawable(drawableResId);
        if(upArrow != null) {
            upArrow.setColorFilter(getResources().getColor(drawableColorFilter), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(enabled);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
        }
    }

    public void setDisplayHomeAsUpEnabled(boolean enabled) {
        setDisplayHomeAsUpEnabled(true, R.drawable.ic_back_arrow, R.color.md_white_1000);
    }

    /**
     * Enable or disable the home button on the toolbar with some customization.
     * @param enabled Set the home button to be enabled or disabled
     * @param drawableResId A resource id for the button {@link Drawable}
     */
    public void setDisplayHomeAsUpEnabled(boolean enabled, int drawableResId) {
        setDisplayHomeAsUpEnabled(enabled, drawableResId, R.color.md_white_1000);
    }

    /**
     * Create a {@link Snackbar}
     * @param message The message of the snackbar
     * @param length The length of the snackbar's appearance
     */
    public Snackbar createSnackbar(String message, int length) {
        View root = findViewById(android.R.id.content);
        if(root != null) {
            return Snackbar.make(root, message, length);
        } else return null;
    }

    /**
     * Show a {@link Snackbar} at the bottom of the activity
     * @param message The message of the snackbar
     * @param length The length of the snackbar's appearance
     */
    public void snackbar(String message, int length) {
        Snackbar snackbar = createSnackbar(message, length);
        if(snackbar == null) {
            Toast.makeText(MaterialActivity.this, message, length).show();
        } else {
            snackbar.show();
        }
    }

    public Bitmap getImageFromAssets(String imageName) {
        try {
            AssetManager assets = getAssets();
//            String path = getPathNameForFile(assets, "", imageName);
            String path = imageName;
            InputStream stream = assets.open(path);
            Bitmap bmp = BitmapFactory.decodeStream(stream);
            return bmp;
        } catch(IOException ex) {
            return null;
        }
    }

    private String getPathNameForFile(AssetManager mgr, String path, String fileName) {
        if(path.endsWith(fileName)) {
            return path;
        }
        try {
            String[] paths = mgr.list(path);
            if(paths.length > 0) {
                for(String newPath : paths) {
                    String returned = getPathNameForFile(mgr, path + "\\" + newPath, fileName);
                    if(returned != null) {
                        return returned;
                    }
                }
            } else {
                Log.e("MaterialActivity", "File list empty");
            }
        } catch(IOException ex) {
            Log.e("MaterialActivity", "Cant reach path");
        }
        return null;
    }

}
